---
title: "Archäologischer Bestimmungsschlüssel: B1 Gefäße"
subtitle: "--- basierend auf der SHKR Typologie (Nakoinz 2013)"
author: '[Prof. Dr. Oliver Nakoinz](http://oliver.nakoinz.gitlab.io/OliverNakoinz/)'
date: "Version 6, August 2022 <br>  <a rel='license' href='http://creativecommons.org/licenses/by-sa/4.0/'><img alt='Creative Commons License' style='border-width:0' src='https://i.creativecommons.org/l/by-sa/4.0/88x31.png' /></a><br />Dieses Werk ist lizenziert unter einer <a rel='license' href='http://creativecommons.org/licenses/by-sa/4.0/'>Creative Commons Attribution-ShareAlike 4.0 International Lizenz</a>. "
lang: de-DE 
otherlangs: en-GB
#nocite: '@*'
output:
  slidy_presentation:
    highlight: tango
    font_adjustment: +0
    #pandoc_args:
    #- --css
    #- stycss/styles_slidy_bestimmungsschluessel.css
    footer: "Oliver Nakoinz  <a rel='license' href='http://creativecommons.org/licenses/by-sa/4.0/'><img alt='Creative Commons License' style='border-width:0' src='https://i.creativecommons.org/l/by-sa/4.0/80x15.png' /></a>  SHKR 6 Klassifikation"
    df_print: kable
fontsize: 13pt
font-family: 'Helvetica'
widescreen: true
self_contained: no
Comment: xxxxxxxxxxxxxxxxxxxxxx
---


# B6 Geräte  <!-- F0002 --> 


<!-- 
pandoc -t slidy -s /home/fon/daten/db/shkrtypology/BestSchl_B6_de.md -o /home/fon/daten/db/shkrtypology/public/BestSchl_B6_de.html

git add .
git commit -m "new content"
git push
 --> 


[zurück](BestSchl_de.html#(4))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

xxx


# Literatur
