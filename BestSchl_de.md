---
title: "Archäologischer Bestimmungsschlüssel"
subtitle: "--- basierend auf der SHKR Typologie (Nakoinz 2013)"
author: '[Prof. Dr. Oliver Nakoinz](http://oliver.nakoinz.gitlab.io/OliverNakoinz/)'
date: "Version 6, August 2022 <br>  <a rel='license' href='http://creativecommons.org/licenses/by-sa/4.0/'><img alt='Creative Commons License' style='border-width:0' src='https://i.creativecommons.org/l/by-sa/4.0/88x31.png' /></a><br />Dieses Werk ist lizenziert unter einer <a rel='license' href='http://creativecommons.org/licenses/by-sa/4.0/'>Creative Commons Attribution-ShareAlike 4.0 International Lizenz</a>. "
lang: de-DE
otherlangs:  en-GB
#nocite: '@*'
output:
  slidy_presentation:
    highlight: tango
    font_adjustment: +0
    #pandoc_args:
    #- --css
    #- stycss/styles_slidy_bestimmungsschluessel.css
    footer: "Oliver Nakoinz  <a rel='license' href='http://creativecommons.org/licenses/by-sa/4.0/'><img alt='Creative Commons License' style='border-width:0' src='https://i.creativecommons.org/l/by-sa/4.0/80x15.png' /></a> SHKR 6 Klassifikation"
    df_print: kable
fontsize: 13pt
font-family: 'Helvetica'
widescreen: true
self_contained: yes
Comment: xxxxxxxxxxxxxxxxxxxxxx
#duration: 30
---

# Kategorien <!-- F0002 --> 

<!-- 
pandoc -t slidy -s /home/fon/daten/db/shkrtypology/BestSchl_de.md -o /home/fon/daten/db/shkrtypology/public/BestSchl_de.html

git add .
git commit -m "new content"
git push
 --> 

Die vorliegende Klassifikation archäologischer Funde basiert auf der SHKR Klassifikation zur älteren Eisenzeit nach Nakoinz (2013) und wurde rückwärteskompatible weiterentwickelt. Es handelt sich um eine hierarchische Fazettenklassifikation mit den unten aufgeführten Fazetten. Teile der Klassifikation sind insopmorph und verzweigen sich demnach in gleicher Weise und mit den gleichen untergeordneten Merkmalen weiter. Hierfür werden in der [Übersicht](index.html) Punkte als Platzhalter verwendet. Platzhalter kommen im Bestimmungsschlüssel nicht vor.

- Fundmaterial
  - A Material
  - [B Formen](BestSchl_de.html#(3))
  - C Ziermotive
  - D Ziertechniken
- Befunde


# B Formen <!-- F0003 --> 

[zurück](BestSchl_de.html#(2))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

- [B1 Gefäße](BestSchl_B1_de.html#(2))
- [B2 Ringschmuck](BestSchl_B2_de.html#(2))
- [B3 Kleidung und Kleidungszubehör](BestSchl_B3_de.html#(2))
- [B4 Waffen](BestSchl_B4_de.html#(2))
- [B5 Verkehrsmittel](BestSchl_B5_de.html#(2))
- [B6 Geräte](BestSchl_B6_de.html#(2))
- [B7 Zier- und Kunstobjekte](BestSchl_B7_de.html#(2))
- [B8 Produktion und Bau](BestSchl_B8_de.html#(2))
- [B9 Verschiedenes](BestSchl_B9_de.html#(2))


# Literatur

Nakoinz 2013: O. Nakoinz, Archäologische Kulturgeographie der ältereisenzeitlichen Zentralorte Südwestdeutschlands 224. Universitätsforsch. Prähist. Arch. (Bonn 2013).
