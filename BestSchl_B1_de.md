---
title: "Archäologischer Bestimmungsschlüssel: B1 Gefäße"
subtitle: "--- basierend auf der SHKR Typologie (Nakoinz 2013)"
author: '[Prof. Dr. Oliver Nakoinz](http://oliver.nakoinz.gitlab.io/OliverNakoinz/)'
date: "Version 6, August 2022 <br>  <a rel='license' href='http://creativecommons.org/licenses/by-sa/4.0/'><img alt='Creative Commons License' style='border-width:0' src='https://i.creativecommons.org/l/by-sa/4.0/88x31.png' /></a><br />Dieses Werk ist lizenziert unter einer <a rel='license' href='http://creativecommons.org/licenses/by-sa/4.0/'>Creative Commons Attribution-ShareAlike 4.0 International Lizenz</a>. "
lang: de-DE 
otherlangs: en-GB
#nocite: '@*'
output:
  slidy_presentation:
    highlight: tango
    font_adjustment: +0
    #pandoc_args:
    #- --css
    #- stycss/styles_slidy_bestimmungsschluessel.css
    footer: "Oliver Nakoinz  <a rel='license' href='http://creativecommons.org/licenses/by-sa/4.0/'><img alt='Creative Commons License' style='border-width:0' src='https://i.creativecommons.org/l/by-sa/4.0/80x15.png' /></a>  SHKR 6 Klassifikation"
    df_print: kable
fontsize: 13pt
font-family: 'Helvetica'
widescreen: true
self_contained: no
Comment: xxxxxxxxxxxxxxxxxxxxxx
---


# B1 Gefäße  <!-- F0002 --> 

<!-- 
pandoc -t slidy -s /home/fon/daten/db/shkrtypology/BestSchl_B1_de.md -o /home/fon/daten/db/shkrtypology/public/BestSchl_B1_de.html

git add .
git commit -m "new content"
git push
 --> 

[zurück](BestSchl_de.html#(3))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

<div class="column-left-big">
- [B11 Hochform](BestSchl_B1_de.html#(3))	
    - Gefäßhöhe >= max Gefäßdurchmesser 
</div>

<div class="column-right-small">
![](./4figures/B11.png){ height=120px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12 Breitform](BestSchl_B1_de.html#(4))	
    - Gefäßhöhe < max Gefäßdurchmesser 
</div>


<div class="column-right-small">
![](./4figures/B12.png){ height=120px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B197 Sonstige](BestSchl_B1_de.html#(5))	
    - alle Sonderformen
</div>


# B11 Hochform  <!-- F0003 --> 

*Gefäß>Hochform*

![](./4figures/B11.png){ height=80px}

[zurück](BestSchl_B1_de.html#(2))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Grundform**

<div class="column-left-big">
- [B111 Grundform 1](BestSchl_B1_de.html#(6))	
  - runde Wandung
  - 1-2 gliedrig
  - weitgehend konkav
  - kalottenförmig
</div>

<div class="column-right-small">
![](./4figures/B1x1.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B112 Grundform 2](BestSchl_B1_de.html#(7))	
  - abgesetzter Hals
  - 3-gliedrig
  - Kegelhals, Zylinderhals
</div>

<div class="column-right-small">
![](./4figures/B1x2.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B113 Grundform 3](BestSchl_B1_de.html#(8))	
  - gerade Wandung
  - eimerartig
</div>


<div class="column-right-small">
![](./4figures/B1x3.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B114 Grundform 4](BestSchl_B1_de.html#(9))	
  - Knickwand
  - Knick nicht an der breitesten Stelle
</div>

<div class="column-right-small">
![](./4figures/B1x4.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B115 Grundform 5](BestSchl_B1_de.html#(10))	
  - Bauchknick
  - Knick an der breitesten Stelle
</div>

<div class="column-right-small">
![](./4figures/B1x5.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B116 Grundform 6](BestSchl_B1_de.html#(11))	
  - Metallgefäße
  - Becken
  - Kessel
  - Kannen
</div>

<div class="column-right-small">
keine Abb. ´
</div>


# B12 Breitform  <!-- F0004 --> 


*Gefäß>Breitform*


![](./4figures/B12.png){ height=80px}

[zurück](BestSchl_B1_de.html#(2))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Grundform**

<div class="column-left-big">
- [B121 Grundform 1](BestSchl_B1_de.html#(12))	
  - runde Wandung
  - 1-2 gliedrig
  - weitgehend konkav
  - kalottenförmig
</div>

<div class="column-right-small">
![](./4figures/B1x1.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>


<div class="column-left-big">
- [B122 Grundform 2](BestSchl_B1_de.html#(13))	
  - abgesetzter Hals
  - 3-gliedrig
  - Kegelhals, Zylinderhals
</div>

<div class="column-right-small">
![](./4figures/B1x2.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>


<div class="column-left-big">
- [B123 Grundform 3](BestSchl_B1_de.html#(14))	
  - gerade Wandung
  - eimerartig
</div>

<div class="column-right-small">
![](./4figures/B1x3.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>


<div class="column-left-big">
- [B124 Grundform 4](BestSchl_B1_de.html#(15))	
  - Knickwand
  - Knick nicht an der breitesten Stelle

</div>

<div class="column-right-small">
![](./4figures/B1x4.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B125 Grundform 5](BestSchl_B1_de.html#(16))	
  - Bauchknick
  - Knick an der breitesten Stelle
</div>

<div class="column-right-small">
![](./4figures/B1x5.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B126 Grundform 6](BestSchl_B1_de.html#(17))	
  - Metallgefäße
  - Becken
  - Kessel
  - Kannen
</div>

<div class="column-right-small">
ohne Abb.
</div>


# B197 Sonderformen  <!-- F0005 --> 

*Gefäß>Sonderformen*

[zurück](BestSchl_B1_de.html#(2))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

- [B1971 Trichter](BestSchl_B1_de.html#(xxxx))	
- [B1972 Sieb](BestSchl_B1_de.html#(xxxx))	
- [B1973 Löffel](BestSchl_B1_de.html#(xxxx))	
- [B1974 Tiegel](BestSchl_B1_de.html#(xxxx))	
- [B1975 Deckel](BestSchl_B1_de.html#(xxxx))	
- [B1976 Horn](BestSchl_B1_de.html#(xxxx))	
- [B1977 Briquetage](BestSchl_B1_de.html#(xxxx))	
- [B1978 Scheibe](BestSchl_B1_de.html#(xxxx))	


# B111 Grundform 1  <!-- F0006 --> 

*Gefäß>Hochform>Grundform 1*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x1.png){ height=80px}

[zurück](BestSchl_B1_de.html#(3))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Randform**


<div class="column-left-big">
- [B1111 Kragenrand](BestSchl_B1_de.html#(18))
</div>

<div class="column-right-small">
![](./4figures/B1xx1.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1112 Schrägrand](BestSchl_B1_de.html#(30))
</div>

<div class="column-right-small">
![](./4figures/B1xx2.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1113 S-förmig geschwungen](BestSchl_B1_de.html#(42))	
</div>

<div class="column-right-small">
![](./4figures/B1xx3.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1114 Steilrand](BestSchl_B1_de.html#(54))	
</div>

<div class="column-right-small">
![](./4figures/B1xx4.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1115 gerader Rand](BestSchl_B1_de.html#(66))	
</div>

<div class="column-right-small">
![](./4figures/B1xx5.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1116 eingewölbter Rand](BestSchl_B1_de.html#(78))	
</div>

<div class="column-right-small">
![](./4figures/B1xx6.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1117 eingeknickter Rand](BestSchl_B1_de.html#(90))
</div>

<div class="column-right-small">
![](./4figures/B1xx7.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1118 Blech um Achse](BestSchl_B1_de.html#(102))
</div>

<div class="column-right-small">
![](./4figures/B1xx8.png){ height=80px}
</div>


# B112 Grundform 2  <!-- F0007 --> 


*Gefäß>Hochform>Grundform 2*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x2.png){ height=80px}

[zurück](BestSchl_B1_de.html#(3))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Randform**

<div class="column-left-big">
- [B1121 Zylinderhals](BestSchl_B1_de.html#(19))
</div>

<div class="column-right-small">
![](./4figures/B1xx1.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1122 Schrägrand](BestSchl_B1_de.html#(31))
</div>



<div class="column-right-small">
![](./4figures/B1xx2.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1123 S-förmig geschwungen](BestSchl_B1_de.html#(43))	
</div>

<div class="column-right-small">
![](./4figures/B1xx3.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1124 Steilrand](BestSchl_B1_de.html#(55))	
</div>

<div class="column-right-small">
![](./4figures/B1xx4.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1125 gerader Rand](BestSchl_B1_de.html#(67))	
</div>

<div class="column-right-small">
![](./4figures/B1xx5.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1126 eingewölbter Rand](BestSchl_B1_de.html#(79))	
</div>

<div class="column-right-small">
![](./4figures/B1xx6.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1127 eingeknickter Rand](BestSchl_B1_de.html#(90))
</div>

<div class="column-right-small">
![](./4figures/B1xx7.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1128 Blech um Achse](BestSchl_B1_de.html#(103))
</div>

<div class="column-right-small">
![](./4figures/B1xx8.png){ height=80px}
</div>



# B113 Grundform 3  <!-- F0008 --> 

*Gefäß>Hochform>Grundform 3*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x3.png){ height=80px}

[zurück](BestSchl_B1_de.html#(3))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Randform**

<div class="column-left-big">
- [B1131 Kragenrand](BestSchl_B1_de.html#(20))
</div>

<div class="column-right-small">
![](./4figures/B1xx1.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1132 Schrägrand](BestSchl_B1_de.html#(32))
</div>

<div class="column-right-small">
![](./4figures/B1xx2.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1133 S-förmig geschwungen](BestSchl_B1_de.html#(44))	
</div>

<div class="column-right-small">
![](./4figures/B1xx3.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1134 Steilrand](BestSchl_B1_de.html#(56))	
</div>

<div class="column-right-small">
![](./4figures/B1xx4.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1135 gerader Rand](BestSchl_B1_de.html#(68))	
</div>

<div class="column-right-small">
![](./4figures/B1xx5.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1136 eingewölbter Rand](BestSchl_B1_de.html#(80))	
</div>

<div class="column-right-small">
![](./4figures/B1xx6.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1137 eingeknickter Rand](BestSchl_B1_de.html#(92))
</div>

<div class="column-right-small">
![](./4figures/B1xx7.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1138 Blech um Achse](BestSchl_B1_de.html#(104))
</div>

<div class="column-right-small">
![](./4figures/B1xx8.png){ height=80px}
</div>



# B114 Grundform 4  <!-- F0009 --> 

*Gefäß>Hochform>Grundform 4*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x4.png){ height=80px}

[zurück](BestSchl_B1_de.html#(3))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Randform**

<div class="column-left-big">
- [B1141 Kragenrand](BestSchl_B1_de.html#(21))
</div>

<div class="column-right-small">
![](./4figures/B1xx1.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1142 Schrägrand](BestSchl_B1_de.html#(33))
</div>

<div class="column-right-small">
![](./4figures/B1xx2.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1143 S-förmig geschwungen](BestSchl_B1_de.html#(45))	
</div>

<div class="column-right-small">
![](./4figures/B1xx3.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1144 Steilrand](BestSchl_B1_de.html#(57))	
</div>

<div class="column-right-small">
![](./4figures/B1xx4.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1145 gerader Rand](BestSchl_B1_de.html#(69))	
</div>

<div class="column-right-small">
![](./4figures/B1xx5.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1146 eingewölbter Rand](BestSchl_B1_de.html#(81))	
</div>

<div class="column-right-small">
![](./4figures/B1xx6.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1147 eingeknickter Rand](BestSchl_B1_de.html#(93))
</div>

<div class="column-right-small">
![](./4figures/B1xx7.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1148 Blech um Achse](BestSchl_B1_de.html#(105))
</div>

<div class="column-right-small">
![](./4figures/B1xx8.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

# B115 Grundform 5  <!-- F0010 --> 

*Gefäß>Hochform>Grundform 5*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x5.png){ height=80px}

[zurück](BestSchl_B1_de.html#(3))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Randform**

<div class="column-left-big">
- [B1151 Kragenrand](BestSchl_B1_de.html#(22))
</div>

<div class="column-right-small">
![](./4figures/B1xx1.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1152 Schrägrand](BestSchl_B1_de.html#(34))
</div>

<div class="column-right-small">
![](./4figures/B1xx2.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1153 S-förmig geschwungen](BestSchl_B1_de.html#(46))	
</div>

<div class="column-right-small">
![](./4figures/B1xx3.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1154 Steilrand](BestSchl_B1_de.html#(58))	
</div>

<div class="column-right-small">
![](./4figures/B1xx4.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1155 gerader Rand](BestSchl_B1_de.html#(70))	
</div>

<div class="column-right-small">
![](./4figures/B1xx5.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1156 eingewölbter Rand](BestSchl_B1_de.html#(82))	
</div>

<div class="column-right-small">
![](./4figures/B1xx6.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1157 eingeknickter Rand](BestSchl_B1_de.html#(94))
</div>

<div class="column-right-small">
![](./4figures/B1xx7.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1158 Blech um Achse](BestSchl_B1_de.html#(106))
</div>

<div class="column-right-small">
![](./4figures/B1xx8.png){ height=80px}
</div>

	


# B116 Grundform 6  <!-- F0011 --> 

*Gefäß>Hochform>Grundform 6*

![](./4figures/B11.png){ height=80px}

[zurück](BestSchl_B1_de.html#(3))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Randform**


<div class="column-left-big">
- [B1161 Kragenrand](BestSchl_B1_de.html#(23))
</div>

<div class="column-right-small">
![](./4figures/B1xx1.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1162 Schrägrand](BestSchl_B1_de.html#(35))
</div>

<div class="column-right-small">
![](./4figures/B1xx2.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1163 S-förmig geschwungen](BestSchl_B1_de.html#(47))	
</div>

<div class="column-right-small">
![](./4figures/B1xx3.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1164 Steilrand](BestSchl_B1_de.html#(59))	
</div>

<div class="column-right-small">
![](./4figures/B1xx4.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1165 gerader Rand](BestSchl_B1_de.html#(71))	
</div>

<div class="column-right-small">
![](./4figures/B1xx5.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1166 eingewölbter Rand](BestSchl_B1_de.html#(83))	
</div>

<div class="column-right-small">
![](./4figures/B1xx6.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1167 eingeknickter Rand](BestSchl_B1_de.html#(95))
</div>

<div class="column-right-small">
![](./4figures/B1xx7.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1168 Blech um Achse](BestSchl_B1_de.html#(107))
</div>

<div class="column-right-small">
![](./4figures/B1xx8.png){ height=80px}
</div>

	

# B121 Grundform 1  <!-- F0012 --> 

*Gefäß>Breitform>Grundform 1*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x1.png){ height=80px}

[zurück](BestSchl_B1_de.html#(4))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Randform**

<div class="column-left-big">
- [B1211 Kragenrand](BestSchl_B1_de.html#(24))
</div>

<div class="column-right-small">
![](./4figures/B1xx1.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1212 Schrägrand](BestSchl_B1_de.html#(36))
</div>

<div class="column-right-small">
![](./4figures/B1xx2.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1213 S-förmig geschwungen](BestSchl_B1_de.html#(48))	
</div>

<div class="column-right-small">
![](./4figures/B1xx3.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1214 Steilrand](BestSchl_B1_de.html#(60))	
</div>

<div class="column-right-small">
![](./4figures/B1xx4.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1215 gerader Rand](BestSchl_B1_de.html#(72))	
</div>

<div class="column-right-small">
![](./4figures/B1xx5.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1216 eingewölbter Rand](BestSchl_B1_de.html#(84))	
</div>

<div class="column-right-small">
![](./4figures/B1xx6.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1217 eingeknickter Rand](BestSchl_B1_de.html#(96))
</div>

<div class="column-right-small">
![](./4figures/B1xx7.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1218 Blech um Achse](BestSchl_B1_de.html#(108))
</div>

<div class="column-right-small">
![](./4figures/B1xx8.png){ height=80px}
</div>

		


# B122 Grundform 2  <!-- F0013 --> 

*Gefäß>Breitform>Grundform 2*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x2.png){ height=80px}

[zurück](BestSchl_B1_de.html#(4))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Randform**

<div class="column-left-big">
- [B1221 Kragenrand](BestSchl_B1_de.html#(25))
</div>

<div class="column-right-small">
![](./4figures/B1xx1.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1222 Schrägrand](BestSchl_B1_de.html#(37))
</div>

<div class="column-right-small">
![](./4figures/B1xx2.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1223 S-förmig geschwungen](BestSchl_B1_de.html#(48))	
</div>

<div class="column-right-small">
![](./4figures/B1xx3.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1224 Steilrand](BestSchl_B1_de.html#(61))	
</div>

<div class="column-right-small">
![](./4figures/B1xx4.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1225 gerader Rand](BestSchl_B1_de.html#(73))	
</div>

<div class="column-right-small">
![](./4figures/B1xx5.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1226 eingewölbter Rand](BestSchl_B1_de.html#(85))	
</div>

<div class="column-right-small">
![](./4figures/B1xx6.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1227 eingeknickter Rand](BestSchl_B1_de.html#(97))
</div>

<div class="column-right-small">
![](./4figures/B1xx7.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1228 Blech um Achse](BestSchl_B1_de.html#(109))
</div>

<div class="column-right-small">
![](./4figures/B1xx8.png){ height=80px}
</div>
	


# B123 Grundform 3  <!-- F0014 --> 

*Gefäß>Breitform>Grundform 3*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x3.png){ height=80px}

[zurück](BestSchl_B1_de.html#(4))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Randform**


<div class="column-left-big">
- [B1231 Kragenrand](BestSchl_B1_de.html#(26))
</div>

<div class="column-right-small">
![](./4figures/B1xx1.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1232 Schrägrand](BestSchl_B1_de.html#(38))
</div>

<div class="column-right-small">
![](./4figures/B1xx2.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1233 S-förmig geschwungen](BestSchl_B1_de.html#(50))	
</div>

<div class="column-right-small">
![](./4figures/B1xx3.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1234 Steilrand](BestSchl_B1_de.html#(62))	
</div>

<div class="column-right-small">
![](./4figures/B1xx4.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1235 gerader Rand](BestSchl_B1_de.html#(74))	
</div>

<div class="column-right-small">
![](./4figures/B1xx5.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1236 eingewölbter Rand](BestSchl_B1_de.html#(86))	
</div>

<div class="column-right-small">
![](./4figures/B1xx6.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1237 eingeknickter Rand](BestSchl_B1_de.html#(98))
</div>

<div class="column-right-small">
![](./4figures/B1xx7.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1238 Blech um Achse](BestSchl_B1_de.html#(110))
</div>

<div class="column-right-small">
![](./4figures/B1xx8.png){ height=80px}
</div>



# B124 Grundform 4  <!-- F0015 --> 

*Gefäß>Breitform>Grundform 4*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x4.png){ height=80px}

[zurück](BestSchl_B1_de.html#(4))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Randform**

<div class="column-left-big">
- [B1241 Kragenrand](BestSchl_B1_de.html#(27))
</div>

<div class="column-right-small">
![](./4figures/B1xx1.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1242 Schrägrand](BestSchl_B1_de.html#(39))
</div>

<div class="column-right-small">
![](./4figures/B1xx2.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1243 S-förmig geschwungen](BestSchl_B1_de.html#(51))	
</div>

<div class="column-right-small">
![](./4figures/B1xx3.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1244 Steilrand](BestSchl_B1_de.html#(63))	
</div>

<div class="column-right-small">
![](./4figures/B1xx4.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1245 gerader Rand](BestSchl_B1_de.html#(75))	
</div>

<div class="column-right-small">
![](./4figures/B1xx5.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1246 eingewölbter Rand](BestSchl_B1_de.html#(87))	
</div>

<div class="column-right-small">
![](./4figures/B1xx6.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1247 eingeknickter Rand](BestSchl_B1_de.html#(99))
</div>

<div class="column-right-small">
![](./4figures/B1xx7.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1248 Blech um Achse](BestSchl_B1_de.html#(111))
</div>

<div class="column-right-small">
![](./4figures/B1xx8.png){ height=80px}
</div>


# B125 Grundform 5  <!-- F0016 --> 

*Gefäß>Breitform>Grundform 5*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x5.png){ height=80px}

[zurück](BestSchl_B1_de.html#(4))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Randform**

<div class="column-left-big">
- [B1251 Kragenrand](BestSchl_B1_de.html#(28))
</div>

<div class="column-right-small">
![](./4figures/B1xx1.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1252 Schrägrand](BestSchl_B1_de.html#(40))
</div>

<div class="column-right-small">
![](./4figures/B1xx2.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1253 S-förmig geschwungen](BestSchl_B1_de.html#(52))	
</div>

<div class="column-right-small">
![](./4figures/B1xx3.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1254 Steilrand](BestSchl_B1_de.html#(64))	
</div>

<div class="column-right-small">
![](./4figures/B1xx4.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1255 gerader Rand](BestSchl_B1_de.html#(76))	
</div>

<div class="column-right-small">
![](./4figures/B1xx5.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1256 eingewölbter Rand](BestSchl_B1_de.html#(88))	
</div>

<div class="column-right-small">
![](./4figures/B1xx6.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1257 eingeknickter Rand](BestSchl_B1_de.html#(100))
</div>

<div class="column-right-small">
![](./4figures/B1xx7.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1258 Blech um Achse](BestSchl_B1_de.html#(112))
    -  
</div>

<div class="column-right-small">
![](./4figures/B1xx8.png){ height=80px}
</div>
	


# B126 Grundform 6  <!-- F0017 --> 

*Gefäß>Breitform>Grundform 6*

![](./4figures/B12.png){ height=80px}

[zurück](BestSchl_B1_de.html#(4))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Randform**

<div class="column-left-big">
- [B1261 Kragenrand](BestSchl_B1_de.html#(29))
    -  
</div>

<div class="column-right-small">
![](./4figures/B1xx1.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1262 Schrägrand](BestSchl_B1_de.html#(41))
    -  
</div>

<div class="column-right-small">
![](./4figures/B1xx2.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1263 S-förmig geschwungen](BestSchl_B1_de.html#(53))	
    -  
</div>

<div class="column-right-small">
![](./4figures/B1xx3.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1264 Steilrand](BestSchl_B1_de.html#(65))	
    -  
</div>

<div class="column-right-small">
![](./4figures/B1xx4.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1265 gerader Rand](BestSchl_B1_de.html#(77))	
    -  
</div>

<div class="column-right-small">
![](./4figures/B1xx5.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1266 eingewölbter Rand](BestSchl_B1_de.html#(89))	
    -  
</div>

<div class="column-right-small">
![](./4figures/B1xx6.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1267 eingeknickter Rand](BestSchl_B1_de.html#(101))
    -  
</div>

<div class="column-right-small">
![](./4figures/B1xx7.png){ height=80px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B1268 Blech um Achse](BestSchl_B1_de.html#(113))
    -  
</div>

<div class="column-right-small">
![](./4figures/B1xx8.png){ height=80px}
</div>



# B1111 Kragenrand  <!-- F0018 --> 

*Gefäß>Hochform>Grundform 1>Kragenrand*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x2.png){ height=80px}
![](./4figures/B1xx1.png){ height=80px}

[zurück](BestSchl_B1_de.html#(6))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**

<div class="column-left-big">
- [B11111 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11112 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11114 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11113 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11115 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11116 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1121 Kragenrand  <!-- F0019 --> 

*Gefäß>Hochform>Grundform 2>Kragenrand*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x2.png){ height=80px}
![](./4figures/B1xx1.png){ height=80px}

[zurück](BestSchl_B1_de.html#(7))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11211 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11212 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11214 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11213 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11215 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11216 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1131 Kragenrand  <!-- F0020 --> 

*Gefäß>Hochform>Grundform 3>Kragenrand*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x3.png){ height=80px}
![](./4figures/B1xx1.png){ height=80px}

[zurück](BestSchl_B1_de.html#(8))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11311 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11312 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11314 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11313 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11315 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11316 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1141 Kragenrand  <!-- F0021 --> 

*Gefäß>Hochform>Grundform 4>Kragenrand*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x4.png){ height=80px}
![](./4figures/B1xx1.png){ height=80px}

[zurück](BestSchl_B1_de.html#(9))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11411 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11412 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11414 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11413 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11415 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11416 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1151 Kragenrand  <!-- F0022 --> 

*Gefäß>Hochform>Grundform 5>Kragenrand*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x5.png){ height=80px}
![](./4figures/B1xx1.png){ height=80px}

[zurück](BestSchl_B1_de.html#(10))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11511 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11512 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11514 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11513 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11515 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11516 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1161 Kragenrand  <!-- F0023 --> 

*Gefäß>Hochform>Grundform 6>Kragenrand*

![](./4figures/B11.png){ height=80px}
__
![](./4figures/B1xx1.png){ height=80px}

[zurück](BestSchl_B1_de.html#(11))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11611 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11612 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11614 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11613 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11615 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11616 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1211 Kragenrand  <!-- F0024 --> 

*Gefäß>Breitform>Grundform 1>Kragenrand*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x1.png){ height=80px}
![](./4figures/B1xx1.png){ height=80px}

[zurück](BestSchl_B1_de.html#(12))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12111 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12112 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12114 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12113 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12115 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12116 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1221 Kragenrand  <!-- F0025 --> 

*Gefäß>Breitform>Grundform 2>Kragenrand*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x2.png){ height=80px}
![](./4figures/B1xx1.png){ height=80px}

[zurück](BestSchl_B1_de.html#(13))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12211 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12212 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12214 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12213 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12215 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12216 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1231 Kragenrand  <!-- F0026 --> 

*Gefäß>Breitform>Grundform 3>Kragenrand*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x3.png){ height=80px}
![](./4figures/B1xx1.png){ height=80px}

[zurück](BestSchl_B1_de.html#(14))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12311 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12312 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12314 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12313 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12315 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12316 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1241 Kragenrand  <!-- F0027 --> 

*Gefäß>Breitform>Grundform 4>Kragenrand*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x4.png){ height=80px}
![](./4figures/B1xx1.png){ height=80px}

[zurück](BestSchl_B1_de.html#(15))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12411 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12412 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12414 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12413 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12415 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12416 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1251 Kragenrand  <!-- F0028 --> 

*Gefäß>Breitform>Grundform 5>Kragenrand*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x5.png){ height=80px}
![](./4figures/B1xx1.png){ height=80px}

[zurück](BestSchl_B1_de.html#(16))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12511 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12512 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12514 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12513 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12515 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12516 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1261 Kragenrand  <!-- F0029 --> 

*Gefäß>Breitform>Grundform 6>Kragenrand*

![](./4figures/B12.png){ height=80px}
__
![](./4figures/B1xx1.png){ height=80px}

[zurück](BestSchl_B1_de.html#(17))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12611 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12612 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12614 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12613 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12615 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12616 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1112 Schrägrand  <!-- F0030 --> 

*Gefäß>Hochform>Grundform 1>Schrägrand*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x2.png){ height=80px}
![](./4figures/B1xx2.png){ height=80px}

[zurück](BestSchl_B1_de.html#(6))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**

<div class="column-left-big">
- [B11121 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11122 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11124 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11123 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11125 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11126 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1122 Schrägrand  <!-- F0031 --> 

*Gefäß>Hochform>Grundform 2>Schrägrand*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x2.png){ height=80px}
![](./4figures/B1xx2.png){ height=80px}

[zurück](BestSchl_B1_de.html#(7))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11221 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11222 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11224 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11223 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11225 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11226 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1132 Schrägrand  <!-- F0032 --> 

*Gefäß>Hochform>Grundform 3>Schrägrand*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x3.png){ height=80px}
![](./4figures/B1xx2.png){ height=80px}

[zurück](BestSchl_B1_de.html#(8))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11321 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11322 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11324 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11323 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11325 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11326 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1142 Schrägrand  <!-- F0033 --> 

*Gefäß>Hochform>Grundform 4>Schrägrand*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x4.png){ height=80px}
![](./4figures/B1xx2.png){ height=80px}

[zurück](BestSchl_B1_de.html#(9))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11421 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11422 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11424 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11423 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11425 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11426 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1152 Schrägrand  <!-- F0034 --> 

*Gefäß>Hochform>Grundform 5>Schrägrand*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x5.png){ height=80px}
![](./4figures/B1xx2.png){ height=80px}

[zurück](BestSchl_B1_de.html#(10))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11521 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11522 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11524 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11523 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11525 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11526 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1162 Schrägrand  <!-- F0035 --> 

*Gefäß>Hochform>Grundform 6>Schrägrand*

![](./4figures/B11.png){ height=80px}
__
![](./4figures/B1xx2.png){ height=80px}

[zurück](BestSchl_B1_de.html#(11))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11621 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11622 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11624 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11623 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11625 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11626 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1212 Schrägrand  <!-- F0036 --> 

*Gefäß>Breitform>Grundform 1>Schrägrand*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x1.png){ height=80px}
![](./4figures/B1xx2.png){ height=80px}

[zurück](BestSchl_B1_de.html#(12))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12121 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12122 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12124 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12123 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12125 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12126 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1222 Schrägrand  <!-- F0037 --> 

*Gefäß>Breitform>Grundform 2>Schrägrand*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x2.png){ height=80px}
![](./4figures/B1xx2.png){ height=80px}

[zurück](BestSchl_B1_de.html#(13))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12221 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12222 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12224 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12223 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12225 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12226 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1232 Schrägrand  <!-- F0038 --> 

*Gefäß>Breitform>Grundform 3>Schrägrand*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x3.png){ height=80px}
![](./4figures/B1xx2.png){ height=80px}

[zurück](BestSchl_B1_de.html#(14))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12321 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12322 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12324 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12323 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12325 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12326 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1242 Schrägrand  <!-- F0039 --> 

*Gefäß>Breitform>Grundform 4>Schrägrand*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x4.png){ height=80px}
![](./4figures/B1xx2.png){ height=80px}

[zurück](BestSchl_B1_de.html#(15))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12421 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12422 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12424 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12423 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12425 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12426 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1252 Schrägrand  <!-- F0040 --> 

*Gefäß>Breitform>Grundform 5>Schrägrand*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x5.png){ height=80px}
![](./4figures/B1xx2.png){ height=80px}

[zurück](BestSchl_B1_de.html#(16))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12521 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12522 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12524 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12523 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12525 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12526 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1262 Schrägrand  <!-- F0041 --> 

*Gefäß>Breitform>Grundform 6>Schrägrand*

![](./4figures/B12.png){ height=80px}
__
![](./4figures/B1xx2.png){ height=80px}

[zurück](BestSchl_B1_de.html#(17))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12621 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12622 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12624 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12623 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12625 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12626 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1113 S-förmig geschwungen  <!-- F0042 --> 

*Gefäß>Hochform>Grundform 1>S-förmig geschwungen*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x2.png){ height=80px}
![](./4figures/B1xx3.png){ height=80px}

[zurück](BestSchl_B1_de.html#(6))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**

<div class="column-left-big">
- [B11131 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11132 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11134 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11133 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11135 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11136 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1123 S-förmig geschwungen  <!-- F0043 --> 

*Gefäß>Hochform>Grundform 2>S-förmig geschwungen*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x2.png){ height=80px}
![](./4figures/B1xx3.png){ height=80px}

[zurück](BestSchl_B1_de.html#(7))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11231 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11232 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11234 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11233 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11235 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11236 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1133 S-förmig geschwungen  <!-- F0044 --> 

*Gefäß>Hochform>Grundform 3>S-förmig geschwungen*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x3.png){ height=80px}
![](./4figures/B1xx3.png){ height=80px}

[zurück](BestSchl_B1_de.html#(8))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11331 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11332 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11334 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11333 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11335 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11336 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1143 S-förmig geschwungen  <!-- F0045 --> 

*Gefäß>Hochform>Grundform 4>S-förmig geschwungen*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x4.png){ height=80px}
![](./4figures/B1xx3.png){ height=80px}

[zurück](BestSchl_B1_de.html#(9))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11431 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11432 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11434 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11433 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11435 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11436 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1153 S-förmig geschwungen  <!-- F0046 --> 

*Gefäß>Hochform>Grundform 5>S-förmig geschwungen*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x5.png){ height=80px}
![](./4figures/B1xx3.png){ height=80px}

[zurück](BestSchl_B1_de.html#(10))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11531 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11532 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11534 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11533 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11535 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11536 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1163 S-förmig geschwungen  <!-- F0047 --> 

*Gefäß>Hochform>Grundform 6>S-förmig geschwungen*

![](./4figures/B11.png){ height=80px}
__
![](./4figures/B1xx3.png){ height=80px}

[zurück](BestSchl_B1_de.html#(11))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11631 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11632 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11634 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11633 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11635 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11636 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1213 S-förmig geschwungen  <!-- F0048 --> 

*Gefäß>Breitform>Grundform 1>S-förmig geschwungen*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x1.png){ height=80px}
![](./4figures/B1xx3.png){ height=80px}

[zurück](BestSchl_B1_de.html#(12))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12131 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12132 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12134 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12133 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12135 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12136 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1223 S-förmig geschwungen  <!-- F0048 --> 

*Gefäß>Breitform>Grundform 2>S-förmig geschwungen*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x2.png){ height=80px}
![](./4figures/B1xx3.png){ height=80px}

[zurück](BestSchl_B1_de.html#(13))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12231 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12232 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12234 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12233 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12235 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12236 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1233 S-förmig geschwungen  <!-- F0050 --> 

*Gefäß>Breitform>Grundform 3>S-förmig geschwungen*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x3.png){ height=80px}
![](./4figures/B1xx3.png){ height=80px}

[zurück](BestSchl_B1_de.html#(14))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12331 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12332 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12334 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12333 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12335 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12336 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1243 S-förmig geschwungen  <!-- F0051 --> 

*Gefäß>Breitform>Grundform 4>S-förmig geschwungen*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x4.png){ height=80px}
![](./4figures/B1xx3.png){ height=80px}

[zurück](BestSchl_B1_de.html#(15))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12431 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12432 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12434 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12433 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12435 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12436 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1253 S-förmig geschwungen  <!-- F0052 --> 

*Gefäß>Breitform>Grundform 5>S-förmig geschwungen*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x5.png){ height=80px}
![](./4figures/B1xx3.png){ height=80px}

[zurück](BestSchl_B1_de.html#(16))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12531 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12532 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12534 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12533 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12535 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12536 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1263 S-förmig geschwungen  <!-- F0053 --> 

*Gefäß>Breitform>Grundform 6>S-förmig geschwungen*

![](./4figures/B12.png){ height=80px}
__
![](./4figures/B1xx3.png){ height=80px}

[zurück](BestSchl_B1_de.html#(17))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12631 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12632 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12634 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12633 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12635 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12636 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1114 Steilrand  <!-- F0054 --> 

*Gefäß>Hochform>Grundform 1>Steilrand*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x2.png){ height=80px}
![](./4figures/B1xx4.png){ height=80px}

[zurück](BestSchl_B1_de.html#(6))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**

<div class="column-left-big">
- [B11141 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11142 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11144 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11143 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11145 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11146 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1124 Steilrand  <!-- F0055 --> 

*Gefäß>Hochform>Grundform 2>Steilrand*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x2.png){ height=80px}
![](./4figures/B1xx4.png){ height=80px}

[zurück](BestSchl_B1_de.html#(7))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11241 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11242 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11244 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11243 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11245 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11246 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1134 Steilrand  <!-- F0056 --> 

*Gefäß>Hochform>Grundform 3>Steilrand*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x3.png){ height=80px}
![](./4figures/B1xx4.png){ height=80px}

[zurück](BestSchl_B1_de.html#(8))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11341 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11342 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11344 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11343 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11345 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11346 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1144 Steilrand  <!-- F0057 --> 

*Gefäß>Hochform>Grundform 4>Steilrand*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x4.png){ height=80px}
![](./4figures/B1xx4.png){ height=80px}

[zurück](BestSchl_B1_de.html#(9))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11441 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11442 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11444 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11443 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11445 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11446 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1154 Steilrand  <!-- F0058 --> 

*Gefäß>Hochform>Grundform 5>Steilrand*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x5.png){ height=80px}
![](./4figures/B1xx4.png){ height=80px}

[zurück](BestSchl_B1_de.html#(10))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11541 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11542 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11544 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11543 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11545 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11546 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1164 Steilrand  <!-- F0059 --> 

*Gefäß>Hochform>Grundform 6>Steilrand*

![](./4figures/B11.png){ height=80px}
__
![](./4figures/B1xx4.png){ height=80px}

[zurück](BestSchl_B1_de.html#(11))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11641 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11642 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11644 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11643 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11645 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11646 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1214 Steilrand  <!-- F0060 --> 

*Gefäß>Breitform>Grundform 1>Steilrand*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x1.png){ height=80px}
![](./4figures/B1xx4.png){ height=80px}

[zurück](BestSchl_B1_de.html#(12))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12141 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12142 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12144 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12143 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12145 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12146 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1224 Steilrand  <!-- F0061 --> 

*Gefäß>Breitform>Grundform 2>Steilrand*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x2.png){ height=80px}
![](./4figures/B1xx4.png){ height=80px}

[zurück](BestSchl_B1_de.html#(13))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12241 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12242 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12244 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12243 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12245 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12246 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1234 Steilrand  <!-- F0062 --> 

*Gefäß>Breitform>Grundform 3>Steilrand*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x3.png){ height=80px}
![](./4figures/B1xx4.png){ height=80px}

[zurück](BestSchl_B1_de.html#(14))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12341 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12342 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12344 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12343 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12345 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12346 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1244 Steilrand  <!-- F0063 --> 

*Gefäß>Breitform>Grundform 4>Steilrand*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x4.png){ height=80px}
![](./4figures/B1xx4.png){ height=80px}

[zurück](BestSchl_B1_de.html#(15))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12441 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12442 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12444 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12443 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12445 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12446 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1254 Steilrand  <!-- F0064 --> 

*Gefäß>Breitform>Grundform 5>Steilrand*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x5.png){ height=80px}
![](./4figures/B1xx4.png){ height=80px}

[zurück](BestSchl_B1_de.html#(16))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12541 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12542 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12544 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12543 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12545 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12546 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1264 Steilrand  <!-- F0065 --> 

*Gefäß>Breitform>Grundform 6>Steilrand*

![](./4figures/B12.png){ height=80px}
__
![](./4figures/B1xx4.png){ height=80px}

[zurück](BestSchl_B1_de.html#(17))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12641 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12642 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12644 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12643 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12645 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12646 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1115 gerade  <!-- F0066 --> 

*Gefäß>Hochform>Grundform 1>gerade*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x2.png){ height=80px}
![](./4figures/B1xx5.png){ height=80px}

[zurück](BestSchl_B1_de.html#(6))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**

<div class="column-left-big">
- [B11151 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11152 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11154 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11153 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11155 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11156 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1125 gerade  <!-- F0067 --> 

*Gefäß>Hochform>Grundform 2>gerade*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x2.png){ height=80px}
![](./4figures/B1xx5.png){ height=80px}

[zurück](BestSchl_B1_de.html#(7))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11251 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11252 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11254 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11253 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11255 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11256 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1135 gerade  <!-- F0068 --> 

*Gefäß>Hochform>Grundform 3>gerade*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x3.png){ height=80px}
![](./4figures/B1xx5.png){ height=80px}

[zurück](BestSchl_B1_de.html#(8))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11351 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11352 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11354 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11353 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11355 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11356 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1145 gerade  <!-- F0069 --> 

*Gefäß>Hochform>Grundform 4>gerade*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x4.png){ height=80px}
![](./4figures/B1xx5.png){ height=80px}

[zurück](BestSchl_B1_de.html#(9))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11451 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11452 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11454 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11453 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11455 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11456 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1155 gerade  <!-- F0070 --> 

*Gefäß>Hochform>Grundform 5>gerade*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x5.png){ height=80px}
![](./4figures/B1xx5.png){ height=80px}

[zurück](BestSchl_B1_de.html#(10))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11551 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11552 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11554 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11553 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11555 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11556 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1165 gerade  <!-- F0071 --> 

*Gefäß>Hochform>Grundform 6>gerade*

![](./4figures/B11.png){ height=80px}
__
![](./4figures/B1xx5.png){ height=80px}

[zurück](BestSchl_B1_de.html#(11))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11651 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11652 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11654 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11653 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11655 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11656 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1215 gerade  <!-- F0072 --> 

*Gefäß>Breitform>Grundform 1>gerade*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x1.png){ height=80px}
![](./4figures/B1xx5.png){ height=80px}

[zurück](BestSchl_B1_de.html#(12))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12151 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12152 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12154 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12153 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12155 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12156 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1225 gerade  <!-- F0073 --> 

*Gefäß>Breitform>Grundform 2>gerade*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x2.png){ height=80px}
![](./4figures/B1xx5.png){ height=80px}

[zurück](BestSchl_B1_de.html#(13))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12251 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12252 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12254 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12253 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12255 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12256 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1235 gerade  <!-- F0074 --> 

*Gefäß>Breitform>Grundform 3>gerade*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x3.png){ height=80px}
![](./4figures/B1xx5.png){ height=80px}

[zurück](BestSchl_B1_de.html#(14))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12351 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12352 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12354 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12353 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12355 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12356 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1245 gerade  <!-- F0075 --> 

*Gefäß>Breitform>Grundform 4>gerade*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x4.png){ height=80px}
![](./4figures/B1xx5.png){ height=80px}

[zurück](BestSchl_B1_de.html#(15))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12451 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12452 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12454 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12453 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12455 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12456 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1255 gerade  <!-- F0076 --> 

*Gefäß>Breitform>Grundform 5>gerade*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x5.png){ height=80px}
![](./4figures/B1xx5.png){ height=80px}

[zurück](BestSchl_B1_de.html#(16))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12551 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12552 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12554 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12553 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12555 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12556 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1265 gerade  <!-- F0077 --> 

*Gefäß>Breitform>Grundform 6>gerade*

![](./4figures/B12.png){ height=80px}
__
![](./4figures/B1xx5.png){ height=80px}

[zurück](BestSchl_B1_de.html#(17))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12651 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12652 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12654 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12653 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12655 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12656 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1116 eingewölbt  <!-- F0078 --> 

*Gefäß>Hochform>Grundform 1>eingewölbt*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x2.png){ height=80px}
![](./4figures/B1xx6.png){ height=80px}

[zurück](BestSchl_B1_de.html#(6))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**

<div class="column-left-big">
- [B11161 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11162 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11164 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11163 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11165 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11166 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1126 eingewölbt  <!-- F0079 --> 

*Gefäß>Hochform>Grundform 2>eingewölbt*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x2.png){ height=80px}
![](./4figures/B1xx6.png){ height=80px}

[zurück](BestSchl_B1_de.html#(7))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11261 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11262 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11264 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11263 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11265 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11266 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1136 eingewölbt  <!-- F0080 --> 

*Gefäß>Hochform>Grundform 3>eingewölbt*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x3.png){ height=80px}
![](./4figures/B1xx6.png){ height=80px}

[zurück](BestSchl_B1_de.html#(8))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11361 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11362 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11364 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11363 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11365 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11366 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1146 eingewölbt  <!-- F0081 --> 

*Gefäß>Hochform>Grundform 4>eingewölbt*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x4.png){ height=80px}
![](./4figures/B1xx6.png){ height=80px}

[zurück](BestSchl_B1_de.html#(9))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11461 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11462 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11464 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11463 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11465 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11466 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1156 eingewölbt  <!-- F0082 --> 

*Gefäß>Hochform>Grundform 5>eingewölbt*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x5.png){ height=80px}
![](./4figures/B1xx6.png){ height=80px}

[zurück](BestSchl_B1_de.html#(10))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11561 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11562 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11564 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11563 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11565 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11566 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1166 eingewölbt  <!-- F0083 --> 

*Gefäß>Hochform>Grundform 6>eingewölbt*

![](./4figures/B11.png){ height=80px}
__
![](./4figures/B1xx6.png){ height=80px}

[zurück](BestSchl_B1_de.html#(11))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11661 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11662 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11664 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11663 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11665 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11666 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1216 eingewölbt  <!-- F0084 --> 

*Gefäß>Breitform>Grundform 1>eingewölbt*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x1.png){ height=80px}
![](./4figures/B1xx6.png){ height=80px}

[zurück](BestSchl_B1_de.html#(12))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12161 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12162 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12164 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12163 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12165 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12166 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1226 eingewölbt  <!-- F0085 --> 

*Gefäß>Breitform>Grundform 2>eingewölbt*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x2.png){ height=80px}
![](./4figures/B1xx6.png){ height=80px}

[zurück](BestSchl_B1_de.html#(13))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12261 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12262 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12264 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12263 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12265 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12266 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1236 eingewölbt  <!-- F0086 --> 

*Gefäß>Breitform>Grundform 3>eingewölbt*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x3.png){ height=80px}
![](./4figures/B1xx6.png){ height=80px}

[zurück](BestSchl_B1_de.html#(14))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12361 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12362 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12364 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12363 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12365 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12366 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1246 eingewölbt  <!-- F0087 --> 

*Gefäß>Breitform>Grundform 4>eingewölbt*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x4.png){ height=80px}
![](./4figures/B1xx6.png){ height=80px}

[zurück](BestSchl_B1_de.html#(15))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12461 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12462 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12464 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12463 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12465 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12466 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1256 eingewölbt  <!-- F0088 --> 

*Gefäß>Breitform>Grundform 5>eingewölbt*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x5.png){ height=80px}
![](./4figures/B1xx6.png){ height=80px}

[zurück](BestSchl_B1_de.html#(16))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12561 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12562 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12564 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12563 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12565 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12566 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1266 eingewölbt  <!-- F0089 --> 

*Gefäß>Breitform>Grundform 6>eingewölbt*

![](./4figures/B12.png){ height=80px}
__
![](./4figures/B1xx6.png){ height=80px}

[zurück](BestSchl_B1_de.html#(17))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12661 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12662 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12664 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12663 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12665 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12666 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1117 eingeknickt  <!-- F0090 --> 

*Gefäß>Hochform>Grundform 1>eingeknickt*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x2.png){ height=80px}
![](./4figures/B1xx7.png){ height=80px}

[zurück](BestSchl_B1_de.html#(6))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**

<div class="column-left-big">
- [B11171 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11172 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11174 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11173 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11175 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11176 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1127 eingeknickt  <!-- F0091 --> 

*Gefäß>Hochform>Grundform 2>eingeknickt*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x2.png){ height=80px}
![](./4figures/B1xx7.png){ height=80px}

[zurück](BestSchl_B1_de.html#(7))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11271 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11272 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11274 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11273 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11275 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11276 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1137 eingeknickt  <!-- F0092 --> 

*Gefäß>Hochform>Grundform 3>eingeknickt*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x3.png){ height=80px}
![](./4figures/B1xx7.png){ height=80px}

[zurück](BestSchl_B1_de.html#(8))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11371 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11372 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11374 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11373 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11375 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11376 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1147 eingeknickt  <!-- F0093 --> 

*Gefäß>Hochform>Grundform 4>eingeknickt*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x4.png){ height=80px}
![](./4figures/B1xx7.png){ height=80px}

[zurück](BestSchl_B1_de.html#(9))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11471 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11472 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11474 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11473 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11475 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11476 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1157 eingeknickt  <!-- F0094 --> 

*Gefäß>Hochform>Grundform 5>eingeknickt*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x5.png){ height=80px}
![](./4figures/B1xx7.png){ height=80px}

[zurück](BestSchl_B1_de.html#(10))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11571 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11572 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11574 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11573 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11575 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11576 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1167 eingeknickt  <!-- F0095 --> 

*Gefäß>Hochform>Grundform 6>eingeknickt*

![](./4figures/B11.png){ height=80px}
__
![](./4figures/B1xx7.png){ height=80px}

[zurück](BestSchl_B1_de.html#(11))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11671 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11672 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11674 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11673 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11675 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11676 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1217 eingeknickt  <!-- F0096 --> 

*Gefäß>Breitform>Grundform 1>eingeknickt*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x1.png){ height=80px}
![](./4figures/B1xx7.png){ height=80px}

[zurück](BestSchl_B1_de.html#(12))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12171 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12172 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12174 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12173 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12175 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12176 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1227 eingeknickt  <!-- F0097 --> 

*Gefäß>Breitform>Grundform 2>eingeknickt*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x2.png){ height=80px}
![](./4figures/B1xx7.png){ height=80px}

[zurück](BestSchl_B1_de.html#(13))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12271 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12272 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12274 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12273 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12275 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12276 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1237 eingeknickt  <!-- F0098 --> 

*Gefäß>Breitform>Grundform 3>eingeknickt*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x3.png){ height=80px}
![](./4figures/B1xx7.png){ height=80px}

[zurück](BestSchl_B1_de.html#(14))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12371 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12372 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12374 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12373 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12375 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12376 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1247 eingeknickt  <!-- F0099 --> 

*Gefäß>Breitform>Grundform 4>eingeknickt*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x4.png){ height=80px}
![](./4figures/B1xx7.png){ height=80px}

[zurück](BestSchl_B1_de.html#(15))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12471 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12472 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12474 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12473 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12475 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12476 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1257 eingeknickt  <!-- F0100 --> 

*Gefäß>Breitform>Grundform 5>eingeknickt*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x5.png){ height=80px}
![](./4figures/B1xx7.png){ height=80px}

[zurück](BestSchl_B1_de.html#(16))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12571 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12572 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12574 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12573 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12575 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12576 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1267 eingeknickt  <!-- F0101 --> 

*Gefäß>Breitform>Grundform 6>eingeknickt*

![](./4figures/B12.png){ height=80px}
__
![](./4figures/B1xx7.png){ height=80px}

[zurück](BestSchl_B1_de.html#(17))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12671 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12672 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12674 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12673 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12675 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12676 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1118 Blech um Achse  <!-- F0102 --> 

*Gefäß>Hochform>Grundform 1>Blech um Achse*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x2.png){ height=80px}
![](./4figures/B1xx8.png){ height=80px}

[zurück](BestSchl_B1_de.html#(6))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**

<div class="column-left-big">
- [B11181 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11182 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11184 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11183 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11185 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11186 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1128 Blech um Achse  <!-- F0103 --> 

*Gefäß>Hochform>Grundform 2>Blech um Achse*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x2.png){ height=80px}
![](./4figures/B1xx8.png){ height=80px}

[zurück](BestSchl_B1_de.html#(7))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11281 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11282 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11284 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11283 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11285 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11286 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1138 Blech um Achse  <!-- F0104 --> 

*Gefäß>Hochform>Grundform 3>Blech um Achse*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x3.png){ height=80px}
![](./4figures/B1xx8.png){ height=80px}

[zurück](BestSchl_B1_de.html#(8))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11381 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11382 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11384 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11383 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11385 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11386 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1148 Blech um Achse  <!-- F0105 --> 

*Gefäß>Hochform>Grundform 4>Blech um Achse*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x4.png){ height=80px}
![](./4figures/B1xx8.png){ height=80px}

[zurück](BestSchl_B1_de.html#(9))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11481 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11482 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11484 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11483 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11485 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11486 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1158 Blech um Achse  <!-- F0106 --> 

*Gefäß>Hochform>Grundform 5>Blech um Achse*

![](./4figures/B11.png){ height=80px}
![](./4figures/B1x5.png){ height=80px}
![](./4figures/B1xx8.png){ height=80px}

[zurück](BestSchl_B1_de.html#(10))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11581 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11582 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11584 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11583 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11585 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11586 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1168 Blech um Achse  <!-- F0107 --> 

*Gefäß>Hochform>Grundform 6>Blech um Achse*

![](./4figures/B11.png){ height=80px}
__
![](./4figures/B1xx8.png){ height=80px}

[zurück](BestSchl_B1_de.html#(11))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B11681 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11682 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11684 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11683 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11685 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B11686 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1218 Blech um Achse  <!-- F0108 --> 

*Gefäß>Breitform>Grundform 1>Blech um Achse*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x1.png){ height=80px}
![](./4figures/B1xx8.png){ height=80px}

[zurück](BestSchl_B1_de.html#(12))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12181 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12182 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12184 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12183 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12185 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12186 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1228 Blech um Achse  <!-- F0109 --> 

*Gefäß>Breitform>Grundform 2>Blech um Achse*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x2.png){ height=80px}
![](./4figures/B1xx8.png){ height=80px}

[zurück](BestSchl_B1_de.html#(13))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12281 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12282 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12284 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12283 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12285 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12286 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1238 Blech um Achse  <!-- F0110 --> 

*Gefäß>Breitform>Grundform 3>Blech um Achse*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x3.png){ height=80px}
![](./4figures/B1xx8.png){ height=80px}

[zurück](BestSchl_B1_de.html#(14))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12381 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12382 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12384 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12383 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12385 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12386 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>



# B1248 Blech um Achse  <!-- F0111 --> 

*Gefäß>Breitform>Grundform 4>Blech um Achse*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x4.png){ height=80px}
![](./4figures/B1xx8.png){ height=80px}

[zurück](BestSchl_B1_de.html#(15))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12481 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12482 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12484 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12483 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12485 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12486 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1258 Blech um Achse  <!-- F0112 --> 

*Gefäß>Breitform>Grundform 5>Blech um Achse*

![](./4figures/B12.png){ height=80px}
![](./4figures/B1x5.png){ height=80px}
![](./4figures/B1xx8.png){ height=80px}

[zurück](BestSchl_B1_de.html#(16))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12581 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12582 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12584 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12583 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12585 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12586 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


# B1268 Blech um Achse  <!-- F0113 --> 

*Gefäß>Breitform>Grundform 6>Blech um Achse*

![](./4figures/B12.png){ height=80px}
__
![](./4figures/B1xx8.png){ height=80px}

[zurück](BestSchl_B1_de.html#(17))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Bodenform**



<div class="column-left-big">
- [B12681 Fuß](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx1.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12682 Standring](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx2.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12684 flacher Boden bzw. leicht gewölbt](BestSchl_B1_de.html#(xxxx))
</div>

<div class="column-right-small">
![](./4figures/B1xxx4.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12683 Omphalos](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx3.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12685 runder Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx5.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>

<div class="column-left-big">
- [B12686 spitzer Boden](BestSchl_B1_de.html#(xxxx))	
</div>

<div class="column-right-small">
![](./4figures/B1xxx6.png){ height=100px}
</div>
<hr align="left" width="100%" size="2">>


















# B11111 Fuß  <!-- F0006 --> 

*Gefäß>Hochform>Grundform 1>Kragenrand>Fuß*

[zurück](BestSchl_B1_de.html#(5))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Mündung**

- [B111111 Enghals](BestSchl_B1_de.html#(7))
    - Halsdurchmesser/Bauchdurchmesser < 0,5
- [B111112 mittel/normal](BestSchl_B1_de.html#(xxxx))
    - 0.75 > Halsdurchmesser/Bauchdurchmesser > 0,5
- [B111113 Weithals](BestSchl_B1_de.html#(xxxx))
    - Halsdurchmesser/Bauchdurchmesser > 0,75


# B11111 Enghals  <!-- F0007 --> 

*Gefäß>Hochform>Grundform 1>Kragenrand>Fuß>Enghals*

[zurück](BestSchl_B1_de.html#(6))

<hr align="center" width="100%" size="5"   color="grey" noshade>>

**Schulterumbruch**

- [B1111111 hoher Umbruch](BestSchl_B1_de.html#(7))
    - Umbruchhöhe/Gefäßhöhe > 0.75
- [B1111112 mittelerer Umbruch](BestSchl_B1_de.html#(xxxx))
    - 0.75 > Umbruchhöhe/Gefäßhöhe > 0.5
- [B1111113 niedeiger Umbruch](BestSchl_B1_de.html#(xxxx))
    - Umbruchhöhe/Gefäßhöhe < 0.5

























# Literatur

Nakoinz 2013: O. Nakoinz, Archäologische Kulturgeographie der ältereisenzeitlichen Zentralorte Südwestdeutschlands 224. Universitätsforsch. Prähist. Arch. (Bonn 2013).
